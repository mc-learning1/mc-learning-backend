# mc-learning-backend

Services
---------
CustomerService
LoanService

End Points
----------


#######################
CustomerController#####
#######################

/customer/


#######################################################################################################################################################

/login(POST) (Input : username , password) ,  

RESPONSE:
--------
StatusCode : 200 


-Ve RESPONSE:
-------------
StatusCode : 401  
Description : "Invalid Credentials"


#######################################################################################################################################################

/register(POST) (Input :Name, Username, Password, Address, State, Country, Email Address, PAN, Contact No, DOB, Account Type) - Validation do it in last

RESPONSE:
--------
StatusCode : 201
Description : "Customer registered successfully"

-Ve RESPONSE:
-------------
StatusCode : <<>>  
Description : "User Already Exists"


#######################################################################################################################################################
/updateaccount(PUT) (Input :Name, Username, Password, Address, State, Country, Email Address, PAN, Contact No, DOB, Account Type)
RESPONSE:
--------
StatusCode : 200
Description : "Customer updated successfully"


/getuserdetails/{username}
RESPONSE:
--------
status code(200), Name, Username, Password, Address, State, Country, Email Address, PAN, Contact No, DOB, Account Type
#######################################################################################################################################################


#####################
LoanController#######
#####################

/loan/

#######################################################################################################################################################
/getintrestrate/{loantype}/
RESPONSE:
--------
StatusCode : 200 , Rate of Interest
#######################################################################################################################################################

/apply/{username} (POST) (Input : , Loan Type, Loan Amount, Date, Rate of Interest(Backend db call ie., Not required in the request) and Duration of Loan) - - Validation do it in last
StatusCode : 201 ,
Description :"Loan applied successfully.."


#######################################################################################################################################################

/getloandetails/{username}(GET)

RESPONSE:
--------
status code(200),  (List<>)Loan Types, Loan Amount, Date,Rate of Interest,Duration of Loan 

#######################################################################################################################################################



UI (1st section)
---
( Intial load)

1.	Loan Type – Represents the Loan Type ( Based on loan type - Render additional field)
2.	Loan Amount – Represent the Loan Amount
3.	Loan Apply Date – Represent the Loan Apply Date.
4.	Rate Of interest – Represents the Rate Of interest.
5.	Duration of the loan - Represents the tenure for which the loan is applied for.


if(loantype.equals(Education Loan)){


Education Loan
1.	Course Fee  – Represents  the Course Fee
2.	Course – Represents  the Course
3.	Father Name  – Represents the Father’s Name  
4.	Father Occupation  – Represents  the Father’s Occupation  
5.	Annual Income  – Represents  the Annual Income  

}else{

Personal/home Loan
1.	Annual Income  – Represents the Annual Income  
2.	Company Name  – Represents  the Company Name  
3.	Designation  – Represents  the Designation  
4.	Total Exp  – Represents  the Total Experience
5.	Exp with Current company – Represents the Experience with Current company
}

#######################################################################################################################################################

At last implementation(Needs to be done)
-----------------------
APIGateWay
JWT Token
Validation

#######################################################################################################################################################