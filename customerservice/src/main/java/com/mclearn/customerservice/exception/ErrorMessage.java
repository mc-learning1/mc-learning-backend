package com.mclearn.customerservice.exception;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorMessage {

    private int httpStatus;
    private String message;
    private String errorType;

    /**
     * default constructor.
     */
    public ErrorMessage() {
        super();
    }

    /**
     * constructor.
     *
     * @param message
     * @param httpStatus
     * @param errorType
     */
    public ErrorMessage(final String message,
                        final int httpStatus,
                        final String errorType) {
        super();
        this.message = message;
        this.httpStatus = httpStatus;
        this.errorType = errorType;
    }

}
