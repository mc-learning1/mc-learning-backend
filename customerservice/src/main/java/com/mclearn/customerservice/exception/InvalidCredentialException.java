package com.mclearn.customerservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidCredentialException  extends ResponseStatusException {

    private static final long serialVersionUID = 1224242501759353446L;

    /**
     * Constructor.
     *
     * @param message
     */
    public InvalidCredentialException(final String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }

    /**
     * Constructor.
     *
     * @param message
     * @param cause
     */
    public InvalidCredentialException(final String message, final Throwable cause) {
        super(HttpStatus.BAD_REQUEST, message, cause);
    }

}
