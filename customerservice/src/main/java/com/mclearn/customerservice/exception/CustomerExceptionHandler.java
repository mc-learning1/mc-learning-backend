package com.mclearn.customerservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

import com.mclearn.customerservice.constants.Constants;

@ControllerAdvice
public class CustomerExceptionHandler {

	/**
	 * 
	 * @param exception
	 * @return ResponseEntity.
	 */
    @ExceptionHandler(value = ResponseStatusException.class)
    public ResponseEntity<Object> responseStatusException(final ResponseStatusException exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getReason(), exception.getStatus().value(), exception.getStatus().name());
        return new ResponseEntity<>(errorMessage, exception.getStatus());
    }

    /**
     * 
     * @param exception
     * @return ResponseEntity.
     */
    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> exception(final Exception exception) {
        ErrorMessage errorMessage = new ErrorMessage(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value(), Constants.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
