package com.mclearn.customerservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CustomerNotFoundException  extends ResponseStatusException {

    private static final long serialVersionUID = 1224242501759353446L;

    /**
     * Constructor.
     *
     * @param message
     */
    public CustomerNotFoundException(final String message) {
        super(HttpStatus.NOT_FOUND, message);
    }

    /**
     * Constructor.
     *
     * @param message
     * @param cause
     */
    public CustomerNotFoundException(final String message, final Throwable cause) {
        super(HttpStatus.NOT_FOUND, message, cause);
    }

}
