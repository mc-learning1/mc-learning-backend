package com.mclearn.customerservice.constants;

public final class Constants {

    private Constants() {
    }

    public static final String INTERNAL_SERVER_ERROR = "Internal Server error";

}
