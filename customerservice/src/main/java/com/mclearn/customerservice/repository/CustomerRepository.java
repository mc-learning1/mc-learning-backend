package com.mclearn.customerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mclearn.customerservice.model.CustomerData;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerData, Long>{

	CustomerData findByUserName(String userName);

}
