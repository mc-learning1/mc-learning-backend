package com.mclearn.customerservice.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mclearn.customerservice.model.CustomerData;
import com.mclearn.customerservice.repository.CustomerRepository;

@Service
public class CustomerService {

	private final CustomerRepository customerRepository;
	
	public CustomerService(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	public ResponseEntity<Long> registerCustomer(CustomerData customerData) {
		
		//check if username is already avaialable if yes throw an username already avaialable exception
		//else proceed
		
		customerData = customerRepository.save(customerData);
		return new ResponseEntity<>(customerData.getId(), HttpStatus.CREATED);
	}
	
	public ResponseEntity<CustomerData> getCustomer(String userName) {
		
		CustomerData customerData = customerRepository.findByUserName(userName);
		//check if user is available else throw customer not exception
		return new ResponseEntity<>(customerData, HttpStatus.OK);
	}
	
	
}
