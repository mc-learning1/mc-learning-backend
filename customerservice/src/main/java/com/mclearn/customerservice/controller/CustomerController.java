package com.mclearn.customerservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mclearn.customerservice.model.CustomerCredential;
import com.mclearn.customerservice.model.CustomerData;
import com.mclearn.customerservice.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	
	private final CustomerService customerService;
	
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	/**
	 * 
	 * @param customerCrednetial
	 * @return ResponseEntity.
	 */
	@PostMapping("/login")
    public ResponseEntity<Object> getLogin(@RequestBody CustomerCredential customerCrednetial)
    {
        String username = customerCrednetial.getUserName();
        String pwd = customerCrednetial.getPassword();
        
        //do authentication logic
        
        return ResponseEntity.ok().build();
        
    }
    
    @PostMapping("/register")
    public ResponseEntity<Long> registerCustomer(@RequestBody CustomerData customerData)
    {

    	return customerService.registerCustomer(customerData);
    }

    @GetMapping("/getuserdetails/{username}")
    public ResponseEntity<CustomerData> getUserDetails(@PathVariable("username") String userName)
    {
        
    	return customerService.getCustomer(userName);
        
    }
}
